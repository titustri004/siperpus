/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siperpus;

/**
 *
 * @author titus
 */
// Deklarasi class buku
public class buku {
// Deklarasi Atribut Buku
    private int idBuku;
    private String judulBuku;
    private String penulis;
    private int tahun;
//    Construct
    public buku(int id,String judul,String nama,int thn){
        setIdBuku(id);
        setJudulBuku(judul);
        setPenulis(nama);
        setTahun(thn);
    }
// Setter
    public void setIdBuku(int i){
        idBuku = i;
    }
    public void setJudulBuku(String j) {
        judulBuku = j;
    }
    public void setPenulis(String p) {
        penulis = p;
    }
    public void setTahun(int t) {
        tahun = t;
    }
// Getter
    public int getIdBuku() {
        return  idBuku;
    }
    public String getJudulBuku(){
        return judulBuku;
    }
    public String getPenulis(){
        return penulis;
    }
    public int getTahun(){
        return tahun;
    }
// Tampilkan Data
    public void tampilDataBuku(){
        System.out.println("id Buku: "+ getIdBuku());
        System.out.println("judul Buku: "+ getJudulBuku());
        System.out.println("penulis Buku: "+ getPenulis());
        System.out.println("Tahun Terbit Buku: "+ getTahun());
    }
}
