/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package siperpus;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 *
 * @author titus
 */
public class Siperpus {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean status = true;
        int jmlBuku = 4;
//        Deklarasi Objek Awal untuk dikirim ke construct class buku
        buku[] arrBuku;
        arrBuku = new buku[10];
        arrBuku[0] = new buku(101, "The Archer", "Titus tri P.", 2023);
        arrBuku[1] = new buku(102, "The Commander", "Ali Ridho", 2023);
        arrBuku[2] = new buku(103, "Prediksi pertumbuhan teknologi", "Marcell", 2023);
        arrBuku[3] = new buku(104, "Tips sukses bisnis di era teknologi", "Nofiana", 2023);

//      Deklarasi Objek mahasiswa
        List<mahasiswa> arrMhs = new ArrayList<>();
        arrMhs.add(new mahasiswa("Titus tri P.", 202101,"Teknik"));
        arrMhs.add(new mahasiswa("Ali Ridho", 202102, "Sastra"));
        arrMhs.add(new mahasiswa("Marcell", 202103, "Hukum"));
        arrMhs.add(new mahasiswa("Nofiana", 202104, "Ekonomi dan Bisnis"));


//      Deklarasi Objek pinjaman
        Random rand = new Random();
        List<pinjam> arrPnjm = new ArrayList<>();
        arrPnjm.add(new pinjam(rand.nextInt(1000), "Marcell", "The Archer"));
        
//        Looping
        while (status) {
            int pilihan;

            // Tampilan menu
            System.out.println("SIPERPUS : Aplikasi management perpustakaan");
            System.out.println("-------------------------------------------");
            System.out.println("Pilihan menu:");
            System.out.println("-------------------------------------------");
            System.out.println("1. Daftar Buku");
            System.out.println("2. Daftar Buku Pinjaman");
            System.out.println("3. Daftar Mahasiswa");
            System.out.println("4. Pinjam Buku");
            System.out.println("5. Input Data");
            System.out.println("-------------------------------------------");
            System.out.println("Pilih (6) untuk tutup aplikasi");
            System.out.println("-------------------------------------------");
    //        Deklarasi objeck input data
            Scanner input = new Scanner(System.in);

            //        input data pilihan menu
            System.out.print("Masukkan Pilihan anda: ");
            pilihan = input.nextInt();

    //        Kondisi menu
            switch(pilihan) {
                case 1:
                    System.out.println(" ");
                    System.out.println("Daftar Buku:");
                    System.out.println("-------------------");
                    for (int i = 0; i < jmlBuku; i++) {
                        arrBuku[i].tampilDataBuku();
                        System.out.println("-------------------");
                    }
                    System.out.println("-----------------------");
                    System.out.println("Total jumlah Buku: "+jmlBuku);
                    System.out.println("-----------------------");
                    System.out.println("");
                    System.out.print("Pilih (0) untuk kembali ke menu: ");
                    pilihan = input.nextInt();
                    System.out.println("");
                    break;
                case 2:
//                    Coding nampilin daftar pinjaman buku disini ges
                    System.out.println(" ");
                    System.out.println("Daftar Buku Pinjaman");
                    System.out.println("-------------------");
                    for (int i = 0; i < arrPnjm.size(); i++){
                        pinjam pnj = arrPnjm.get(i);
                        pnj.tampilDataPinjam();
                        System.out.println("-------------------");
                    }
                    System.out.println("-----------------------");
                    System.out.println("Total buku yang dipinjam: "+arrPnjm.size());
                    System.out.println("-----------------------");
                    System.out.println("");
                    System.out.print("Pilih (0) untuk kembali ke menu: ");
                    pilihan = input.nextInt();
                    System.out.println("");
                    break;
                case 3:
                    System.out.println(" ");
                    System.out.println("Daftar Mahasiswa:");
                    System.out.println("-------------------");
                    for (int i = 0; i < arrMhs.size(); i++) {
                        mahasiswa mhs = arrMhs.get(i);
                        mhs.tampilDataMahasiswa();
                        System.out.println("-------------------");
                    }
                    System.out.println("-----------------------");
                    System.out.println("Total jumlah Mahasiswa: "+arrMhs.size());
                    System.out.println("-----------------------");
                    System.out.println("");
                    System.out.print("Pilih (0) untuk kembali ke menu: ");
                    pilihan = input.nextInt();
                    System.out.println("");

                    break;
                case 4:
//                    Coding input pinjaman buku disini ges
                    System.out.println(" ");
                    System.out.println("Pinjam Buku");
                    Boolean mhsLoop = true;
                    while (mhsLoop) {
                        System.out.println("Masukkan NIM: \nKetik (0) untuk kembali ke Menu");
                        int Nim = input.nextInt();
                        if (Nim == 0){
                            mhsLoop = false;
                        }
                        input.nextLine();
                        for (int i = 0; i < arrMhs.size(); i++) {
                            mahasiswa mhs = arrMhs.get(i);
                            if(Nim == mhs.getNim()){
                                System.out.println("NIM ditemukan");
                                System.out.println("-----------------------");
                                Boolean bukuLoop = true;
                                while (bukuLoop) {
                                    System.out.println("Masukkan ID buku yang ingin anda Pinjam : \nKetik (0) untuk kembali ke Menu");
                                    int id = input.nextInt();
                                    if (id == 0){
                                        mhsLoop = false;
                                        bukuLoop = false;
                                    }
                                    input.nextLine();
                                    for (int x = 0; x < jmlBuku; x++){
                                        if(id == arrBuku[x].getIdBuku()){
                                            arrPnjm.add(new pinjam(rand.nextInt(1000), mhs.getNamaMahasiswa(), arrBuku[x].getJudulBuku()));
                                            System.out.println("Peminjaman Buku Sukses");
                                            System.out.println("-----------------------\n");
                                            mhsLoop = false;
                                            bukuLoop = false;
                                        }
                                    }
                                    if (bukuLoop){
                                        System.out.println("Buku tidak ditemukan, silahkan koreksi kembali id buku");
                                        System.out.println("-----------------------\n");
                                    }
                                }
                                break;
                            }
                        }
                        if (mhsLoop) {
                            System.out.println("Mahasiswa tidak ditemukan, silahkan koreksi kembali");
                            System.out.println("-----------------------\n");
                        }
                    }
                    break;
                case 5:
                    System.out.println(" ");
                    System.out.println("Input Data:");
                    System.out.println("-----------------");
                    System.out.println("1. Tambah Data Mahasiswa");
                    System.out.println("2. Tambah Data Buku");
                    System.out.println("-----------------");
                    System.out.print("Masukkan Pilihan anda: ");
                    pilihan = input.nextInt();

                    if (pilihan == 1) {
                        System.out.println(" ");
                        System.out.println("Tambah Data Mahasiswa: ");
                        System.out.println("--------------------");
                        input.nextLine();
                        System.out.print("Masukkan Nama Mahasiswa: ");
                        String nama = input.nextLine();
                        System.out.print("Masukkan NIM: ");
                        long nim = input.nextLong();
                        input.nextLine();
                        System.out.print("Masukkan Fakultas: ");
                        String fakultas = input.nextLine();
                        arrMhs.add(new mahasiswa(nama, nim,fakultas));
                        System.out.println("--------------------------------");
                        System.out.println("Tambah data Mahasiswa berhasil");
                        System.out.println("--------------------------------");
                        System.out.print("Pilih (0) untuk kembali ke menu: ");
                        pilihan = input.nextInt();
                        System.out.println("");

                    } else if (pilihan == 2) {
                        System.out.println(" ");
                        System.out.println("Tambah Data Buku: ");
                        System.out.println("--------------------");
                        input.nextLine();
                        System.out.print("Masukkan Nama Lengkap Penulis: ");
                        String penulis = input.nextLine();
                        System.out.print("Masukkan Judul Buku: ");
                        String judulBuku = input.nextLine();
                        System.out.print("Masukkan Tahun Terbit: ");
                        int tahun = input.nextInt();
                        int idBuku = arrBuku[jmlBuku-1].getIdBuku() + 1; //generate ID Buku
                        arrBuku[jmlBuku] = new buku(idBuku, judulBuku, penulis, tahun);
                        System.out.println("--------------------------------");
                        System.out.println("Tambah data buku berhasil");
                        System.out.println("--------------------------------");
                        jmlBuku += 1;
                        System.out.print("Pilih (0) untuk kembali ke menu: ");
                        pilihan = input.nextInt();
                        System.out.println("");
                    } else {
                        System.out.println("Menu tidak ada!");
                    }
                    break;
                case 0:
                    status = true;
                    break;
                case 6:
                    status = false;
                    break;
                default:
                    System.out.println("Menu tidak ada!");

            }   
            }
    }
    
}
