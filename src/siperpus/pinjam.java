
package siperpus;
/**
 *
 * @author titus
 */
public class pinjam{
    private int kodePinjam;
    private String namaMahasiswa;
    private String namaBuku;
    
    
    public pinjam (int pnjm, String nama_mahasiswa, String nama_buku){
        setKodePinjam (pnjm);
        setNamaMahasiswa (nama_mahasiswa);
        setNamaBuku (nama_buku);
    }

//  setter
    public void setKodePinjam(int kp){
        kodePinjam = kp;
    }
    public void setNamaMahasiswa(String nama_mahasiswa){
        namaMahasiswa = nama_mahasiswa;
    }
    public void setNamaBuku(String nama_buku){
        namaBuku = nama_buku;
    }

//  getter
    public int getKodePinjam(){
        return kodePinjam;
    }
    public String getNamaMahasiswa(){
        return namaMahasiswa;
    }
    public String getNamaBuku(){
        return namaBuku;
    }
    
// tampil data
    public void tampilDataPinjam(){
        System.out.println("Kode Pinjam : " + getKodePinjam());
        System.out.println("Nama Mahasiswa : " + getNamaMahasiswa());
        System.out.println("Judul Buku : " + getNamaBuku());
    }
}
