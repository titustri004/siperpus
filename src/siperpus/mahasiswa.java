/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siperpus;

/**
 *
 * @author titus
 */
public class mahasiswa {
    private String namaMahasiswa;
    private long nim;
    private String fakultas;
    
    public mahasiswa (String m, long n, String ff){
        setNamaMahasiswa(m);
        setNim(n);
        setFakultas(ff);
    }
    
//    Setter
    public void setNamaMahasiswa(String nm){
        namaMahasiswa = nm;
    }
    public void setNim(long n){
        nim = n;
    }
    public void setFakultas(String f){
        fakultas = f;
    }
    
//    Getter
    public String getNamaMahasiswa(){
        return namaMahasiswa;
    }
    public long getNim(){
        return nim;
    }
    public String getFakultas(){
        return fakultas;
    }
    
    public void tampilDataMahasiswa(){
        System.out.println("Nama: "+ getNamaMahasiswa());
        System.out.println("NIM: "+ getNim());
        System.out.println("Fakultas: "+ getFakultas());
    }
}
